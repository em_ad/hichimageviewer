package com.emad.highimageviewer.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.emad.highimageviewer.repo.ImageRepository
import com.emad.highimageviewer.model.Image

class ImageViewModel : ViewModel() {

    private var imagesLiveData : LiveData<PagedList<Image>>? = null

    fun exposeData(): LiveData<PagedList<Image>>?{
        return imagesLiveData
    }

    init {
        imagesLiveData = LivePagedListBuilder(ImageRepository.getImages()!! , 5).build()
    }

    private fun getImages(){
        ImageRepository.getImages()
    }

    init { getImages() }
}

