package com.emad.highimageviewer.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.emad.highimageviewer.R


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().add(
            R.id.flContainer,
            HorizontalImageFragment(), HorizontalImageFragment().javaClass.simpleName).addToBackStack(
            HorizontalImageFragment().javaClass.simpleName).commit()

    }

    override fun onBackPressed() {
        finishAffinity()
    }
}
