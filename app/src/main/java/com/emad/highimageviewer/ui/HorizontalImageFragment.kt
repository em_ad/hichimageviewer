package com.emad.highimageviewer.ui


import android.graphics.Point
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.emad.highimageviewer.ui.adapter.HorizontalImageAdapterPaged
import com.emad.highimageviewer.model.Image
import com.emad.highimageviewer.vm.ImageViewModel
import kotlinx.android.synthetic.main.fragment_horizontal_image.*
import org.greenrobot.eventbus.EventBus
import com.emad.highimageviewer.model.MessageEvent
import org.greenrobot.eventbus.ThreadMode
import org.greenrobot.eventbus.Subscribe
import android.widget.Toast
import com.emad.highimageviewer.R


class HorizontalImageFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? { return inflater.inflate(R.layout.fragment_horizontal_image, container, false) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = HorizontalImageAdapterPaged()
        rvImages.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvImages.adapter = adapter
        rvImages.itemAnimator = DefaultItemAnimator()
        ViewModelProvider(activity!!).get(ImageViewModel::class.java).exposeData()!!.observe(viewLifecycleOwner,object : Observer<PagedList<Image>> {
            override fun onChanged(t: PagedList<Image>?) {
                adapter.submitList(t)
            }
        })
        LinearSnapHelper().attachToRecyclerView(rvImages)
        val param = rvImages.layoutParams
        val p = Point()
        activity!!.window.windowManager.defaultDisplay.getSize(p)
        param.height = p.x
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        Toast.makeText(context, event.message, Toast.LENGTH_SHORT).show()
    }

}
