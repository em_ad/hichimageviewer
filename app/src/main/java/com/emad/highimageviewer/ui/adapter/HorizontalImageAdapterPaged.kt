package com.emad.highimageviewer.ui.adapter

import android.content.Context
import android.graphics.Point
import android.util.Log
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import android.os.CountDownTimer
import android.view.*
import com.emad.highimageviewer.R
import com.emad.highimageviewer.model.Image
import com.github.chrisbanes.photoview.OnScaleChangedListener
import com.github.chrisbanes.photoview.PhotoView
import kotlinx.android.synthetic.main.fragment_horizontal_image.*

class HorizontalImageAdapterPaged :
    PagedListAdapter<Image, HorizontalImageAdapterPaged.ViewHolder>(object :
        DiffUtil.ItemCallback<Image?>() {
        override fun areItemsTheSame(oldItem: Image, newItem: Image): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: Image, newItem: Image): Boolean {
            return oldItem == newItem
        }
    }){

    private lateinit var ctx: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        ctx = parent.context
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_image,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val img: Image = getItem(position) ?: return
        Picasso.get().load(img.url).into(holder.iv)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv: PhotoView
            get() = itemView.findViewById(R.id.ivImage)

        init {
            val c = object : CountDownTimer(400, 400) {
                override fun onTick(l: Long) {

                }

                override fun onFinish() {
                    iv.scale = 1f
                    iv.invalidate()
                }
            }
            iv.setOnDoubleTapListener(null)
            iv.setOnScaleChangeListener(object: OnScaleChangedListener{
                override fun onScaleChange(scaleFactor: Float, focusX: Float, focusY: Float) {
                    c.cancel()
                    c.start()
                }
            })
            iv.isZoomable = true

        }
    }

}