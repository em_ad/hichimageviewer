package com.emad.highimageviewer.util

import android.util.Log
import com.emad.highimageviewer.model.MessageEvent

import org.greenrobot.eventbus.EventBus

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceGenerator {

    private val logLevel = HttpLoggingInterceptor.Level.BODY
    private val httpClient = OkHttpClient.Builder()
    private val logging = HttpLoggingInterceptor()

    private val builder = Retrofit.Builder()
        .baseUrl(Constant.IMAGE_API)
        .addConverterFactory(GsonConverterFactory.create())


    fun <S> createService(serviceClass: Class<S>): S {
        logging.setLevel(logLevel)
        httpClient.interceptors().clear()
        httpClient.addInterceptor(logging)
        httpClient.interceptors().add(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                    .method(original.method, original.body)
                    .header("Accept", "application/json")

                val request = requestBuilder.build()
                val response = chain.proceed(request)
                Log.e("API", request.url.toString() + " -> CODE:" + response.code)
                if (response.code != 200 && response.code != 201) {
                    EventBus.getDefault().post(
                        MessageEvent(
                            "ApiFailure",
                            response.code.toString()
                        )
                    )
                }
                return response
            }
        })
        val client = httpClient.build()
        val retrofit = builder.client(client).build()
        return retrofit.create(serviceClass)
    }
}
