package com.emad.highimageviewer

import android.app.Application

class MyApplication : Application(){

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object{
        lateinit var instance: MyApplication
        fun getApp(): MyApplication{
            return instance
        }
    }

}