package com.emad.highimageviewer.repo

import androidx.paging.DataSource
import com.emad.highimageviewer.util.ServiceGenerator
import com.emad.highimageviewer.model.Image
import com.emad.highimageviewer.repo.database.DBAccess
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ImageRepository {

    companion object {

        var apiRetries = 0
        const val apiRetriesMax = 5

        fun getImages(): DataSource.Factory<Int, Image>? {
            retrieveImages()
            return DBAccess.instance!!.db.accessObject().all
        }

        private fun retrieveImages() {

            val api = ServiceGenerator.createService(API::class.java)
            val call = api.queryImages()
            call.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    val images: ArrayList<Image> = Gson().fromJson(response.body()!!.string(), object : TypeToken<ArrayList<Image>>() {}.type)
//                    Log.e("rag", Gson().toJson(images))
                    DBAccess.instance!!.db.accessObject().insertAll(images)

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    if(apiRetries++ < apiRetriesMax)
                        retrieveImages()
                    else apiRetries = 0
                }
            })
        }
    }
}