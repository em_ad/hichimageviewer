package com.emad.highimageviewer.repo.database

import androidx.room.Database
import androidx.room.RoomDatabase

import com.emad.highimageviewer.util.Constant
import com.emad.highimageviewer.model.Image


@Database(entities = [Image::class], version = Constant.DB_VERSION, exportSchema = false)
abstract class DBInterface : RoomDatabase() {
    abstract fun accessObject(): ImageDao
}
