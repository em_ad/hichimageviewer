package com.emad.highimageviewer.repo.database

import androidx.room.Room

import com.emad.highimageviewer.MyApplication

class DBAccess private constructor() {

    val db: DBInterface = Room.databaseBuilder(MyApplication.instance, DBInterface::class.java, "room_db")
        .allowMainThreadQueries().build()

    init {
        dBaccess = this
    }

    companion object {

        private var dBaccess: DBAccess? = null

        val instance: DBAccess?
            get() {
                if (dBaccess == null)
                    dBaccess = DBAccess()
                return dBaccess
            }
    }
}
