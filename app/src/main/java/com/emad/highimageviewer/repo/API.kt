package com.emad.highimageviewer.repo

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface API {
    @GET("albums/2/photos")
    fun queryImages(): Call<ResponseBody>
}