package com.emad.highimageviewer.repo.database

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import com.emad.highimageviewer.model.Image


@Dao
interface ImageDao {

    @get:Query("SELECT * FROM image_db")
    val all: DataSource.Factory<Int, Image>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(image: Image)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(images: List<Image>)

}
