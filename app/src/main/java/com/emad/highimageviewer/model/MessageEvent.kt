package com.emad.highimageviewer.model

import com.google.gson.annotations.SerializedName

class MessageEvent(message: String?, payload: String?) {

    @SerializedName("message")
    var message: String? = null
    @SerializedName("payload")
    var payload: String? = null


}