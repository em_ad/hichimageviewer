package com.emad.highimageviewer.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "image_db")
class Image{
    @SerializedName("title")
    var title: String? = null
    @SerializedName("url")
    @PrimaryKey
    @NonNull
    var url: String = ""

    override fun equals(other: Any?): Boolean {
        if(other == null) return false
        val image : Image = other as Image
        return title == image.title && url == image.url
    }

    override fun hashCode(): Int {
        var result = title?.hashCode() ?: 0
        result = 31 * result + (url.hashCode())
        return result
    }
}